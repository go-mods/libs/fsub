# FSub

FSub is a small library and cli program to quickly substitute references in text files to other text files with the content of the referenced files.

## Install

Run the following command to add the `fsub` package to your project:

```bash
go get -u gitlab.com/go-mods/libs/fsub
```

### CLI only

If you are only interested in the cli program you can install it by running following command:

```bash
go install gitlab.com/go-mods/libs/fsub/fsub@latest
```

> Alternatively you can find precompiled binaries [here](https://gitlab.com/go-mods/libs/fsub/-/releases) or build from source by yourself.

## How to use

### Library

```golang
// Substitute takes a filepath to a textfile and a regular
// expression, representing the structure of filepaths, as arguments
// substituting any occurences of matches with the regular
// expression inside the textfile with the content of the files the
// matched expressions are referring to. If recursive is set to true
// Substitue will be executed in advance, with the same parameters,
// for each file that is referenced in the textfile.
//
// The regular expression may contain one group to isolate the
// actual filepath within the whole expression. Note that such
// matched paths starting with a forward slash (/) will be treated
// as absolute paths whereas other paths are treated as paths
// relative to the location of the original textfile.
//
// Returns the content from the textfile with substituted filepaths
// as string. In case an error occured the second error return value
// will be unequal to nil.
func Substitute(filepath, expr string, recursive bool) (res string, err error)
```

### CLI

Fsub takes a filepath to a textfile and a [regular expression](https://github.com/google/re2/wiki/Syntax), representing the structure of filepaths, as arguments substituting any occourences of matches with the regular expression inside the textfile with the content of the files the matched expressions are referring to. If the -r flag is provided fsub will be executed in advance, with the same parameters, for each file that is referenced in the textfile.

The regular expression may contain one group to isolate the actual filepath within the whole expression. Note that such matched paths starting with a forward slash (/) will be treated as absolute paths whereas other paths are treated as paths relative to the location of the original textfile.

The content from the textfile with substituted filepaths is printed to stdout.

#### Usage

```bash
fsub (<filepath> <regexp>[ -r|--recursive]) | (-h|--help)
```

#### Examples

```bash
fsub foo.json 'bar\.json'
```

> Replaces all occurrences of 'bar.json' in 'foo.json' with the content of 'bar.json'.

```bash
fsub foo.json '\w+\.json'
```

> Replaces all occurrences of '.json'-files having names made up only of word characters in 'foo.json' with the content of the '.json'-files.

```bash
fsub foo.json '"(\w+\.json)"'
```

> Same as above but file names must be wrapped in quotes. The whole expression (with quotes) is replaced. The actual file name to read from is specified as a group within the round brackets (i.e the text inbetween the quotes).

```bash
fsub foo.json '"(\w+\.json)"' -r
```

> Same as above but also substitutes in '.json'-files referenced inside 'foo.json' and so on.
