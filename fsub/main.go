package main

import (
	"fmt"
	"os"

	"gitlab.com/go-mods/lib/cli"
	"gitlab.com/go-mods/lib/fsub"
)

func main() {
	defer func() { recover() }()

	var hasPath, hasRec, hasHelp bool
	var path, regx string

	cli.FlagsRequired = true
	cli.PrintUsage = PrintUsage
	cli.RegisterFlag("filepath", 0, false, 0x01, &hasPath, 2, &path, &regx)
	cli.RegisterFlag("recursive", 'r', false, 0x02, &hasRec, 0)
	cli.RegisterFlag("help", 'h', false, 0x03, &hasHelp, 0)
	cli.ParseArgs()

	if hasHelp {
		PrintHelp()
	} else {
		result, err := fsub.Substitute(path, regx, hasRec)

		if err != nil {
			os.Stderr.WriteString(err.Error())
		} else {
			os.Stdout.WriteString(result)
		}
	}
}

func PrintUsage() {
	fmt.Println("Usage:\n$ fsub (<filepath> <regexp>[ -r|--recursive]) | (-h|--help)")
}

func PrintHelp() {
	fmt.Printf("Description:\n%s\n%s\n%s\n\n",
		"Fsub takes a filepath to a textfile and a regular expression, representing the structure "+
			"of filepaths, as arguments substituting any occourences of matches with the regular expression "+
			"inside the textfile with the content of the files the matched expressions are referring to. If "+
			"the -r flag is provided fsub will be executed in advance, with the same parameters, for each "+
			"file that is referenced in the textfile.",
		"The regular expression may contain one group to isolate the actual filepath within "+
			"the whole expression. Note that such matched paths starting with a forward slash (/) "+
			"will be treated as absolute paths whereas other paths are treated as paths relative to "+
			"the location of the original textfile.",
		"The content from the textfile with substituted filepaths is printed to stdout.")

	PrintUsage()

	fmt.Println("\nExamples:")
	fmt.Printf("$ %s\n%s\n\n", "fsub foo.json 'bar\\.json'", "Replaces all occurrences of 'bar.json' in 'foo.json' with the content of 'bar.json'.")
	fmt.Printf("$ %s\n%s\n\n", "fsub foo.json '\\w+\\.json'", "Replaces all occurrences of '.json'-files having names made up only of word characters in 'foo.json' with the content of the '.json'-files.")
	fmt.Printf("$ %s\n%s\n\n", "fsub foo.json '\"(\\w+\\.json)\"'", "Same as above but file names must be wrapped in quotes. The whole expression (with quotes) is replaced. The actual file name to read from is specified as a group within the round brackets (i.e the text inbetween the quotes).")
	fmt.Printf("$ %s\n%s\n", "fsub foo.json '\"(\\w+\\.json)\"' -r", "Same as above but also substitutes in '.json'-files referenced inside 'foo.json' and so on.")
}
